.PHONY: build clean

build: test.exe

clean:
	rm -f test.exe

test.exe: test.c reglob.c reglob.h
	clang -fsyntax-only -Weverything test.c reglob.c
	gcc -std=c99 -pedantic -O3 -o $@ test.c reglob.c
