#include "reglob.h"
#include <string.h>

enum { SPECIAL, META, REGULAR };
static const int CHARMAP[256] = {
    [' ' ] = REGULAR,   ['!' ] = META,      ['"' ] = META,
    ['#' ] = META,      ['$' ] = META,      ['%' ] = META,
    ['&' ] = META,      ['\''] = META,      ['(' ] = META,
    [')' ] = META,      ['*' ] = META,      ['+' ] = META,
    [',' ] = META,      ['-' ] = META,      ['.' ] = META,
    ['/' ] = META,      ['0' ] = REGULAR,   ['1' ] = REGULAR,
    ['2' ] = REGULAR,   ['3' ] = REGULAR,   ['4' ] = REGULAR,
    ['5' ] = REGULAR,   ['6' ] = REGULAR,   ['7' ] = REGULAR,
    ['8' ] = REGULAR,   ['9' ] = REGULAR,   [':' ] = META,
    [';' ] = META,      ['<' ] = META,      ['=' ] = META,
    ['>' ] = META,      ['?' ] = META,      ['@' ] = META,
    ['A' ] = REGULAR,   ['B' ] = REGULAR,   ['C' ] = REGULAR,
    ['D' ] = REGULAR,   ['E' ] = REGULAR,   ['F' ] = REGULAR,
    ['G' ] = REGULAR,   ['H' ] = REGULAR,   ['I' ] = REGULAR,
    ['J' ] = REGULAR,   ['K' ] = REGULAR,   ['L' ] = REGULAR,
    ['M' ] = REGULAR,   ['N' ] = REGULAR,   ['O' ] = REGULAR,
    ['P' ] = REGULAR,   ['Q' ] = REGULAR,   ['R' ] = REGULAR,
    ['S' ] = REGULAR,   ['T' ] = REGULAR,   ['U' ] = REGULAR,
    ['V' ] = REGULAR,   ['W' ] = REGULAR,   ['X' ] = REGULAR,
    ['Y' ] = REGULAR,   ['Z' ] = REGULAR,   ['[' ] = META,
    ['\\'] = META,      [']' ] = META,      ['^' ] = META,
    ['_' ] = REGULAR,   ['`' ] = META,      ['a' ] = REGULAR,
    ['b' ] = REGULAR,   ['c' ] = REGULAR,   ['d' ] = REGULAR,
    ['e' ] = REGULAR,   ['f' ] = REGULAR,   ['g' ] = REGULAR,
    ['h' ] = REGULAR,   ['i' ] = REGULAR,   ['j' ] = REGULAR,
    ['k' ] = REGULAR,   ['l' ] = REGULAR,   ['m' ] = REGULAR,
    ['n' ] = REGULAR,   ['o' ] = REGULAR,   ['p' ] = REGULAR,
    ['q' ] = REGULAR,   ['r' ] = REGULAR,   ['s' ] = REGULAR,
    ['t' ] = REGULAR,   ['u' ] = REGULAR,   ['v' ] = REGULAR,
    ['w' ] = REGULAR,   ['x' ] = REGULAR,   ['y' ] = REGULAR,
    ['z' ] = REGULAR,   ['{' ] = META,      ['|' ] = META,
    ['}' ] = META,      ['~' ] = META,
};

static int hi(char cc) { return (unsigned char)cc >> 4; }
static int lo(char cc) { return (unsigned char)cc & 0xF; }

static char to_hex(int code) {
    if(code <= 0x9) return (char)('0' + code);
    if(code <= 0xF) return (char)('A' + (code - 0xA));
    return 0;
}

static void push(char **cursor, char cc) { *(*cursor)++ = cc; }

static void push_string(char **cursor, const char *string, size_t len) {
    memcpy(*cursor, string, len);
    *cursor += len;
}

static void push_encoded(char **cursor, char cc) {
    switch(CHARMAP[(unsigned char)cc]) {
        case SPECIAL:
            push_string(cursor, (char []){
                '\\', 'x', to_hex(hi(cc)), to_hex(lo(cc))
            }, 4);
            break;
        case META:
            push(cursor, '\\');
            push(cursor, cc);
            break;
        case REGULAR:
            push(cursor, cc);
            break;
    }
}

static void push_escape(char **cursor, char cc) {
    if(CHARMAP[(unsigned char)cc] == REGULAR) {
        push(cursor, '\\');
        push(cursor, cc);
    }
    else push_encoded(cursor, cc);
}

char *reglob(char *buffer, const char *pattern, _Bool root) {
    char *cursor[1] = { buffer };
    char cc;

    if(root) push(cursor, '^');

    NEXT: for(;;) {
        cc = *pattern++;
        switch(cc) {
            case 0:
                if(root) push(cursor, '$');
                push(cursor, 0);
                return buffer;
            case '?':
                push_string(cursor, "[^\\/]", 5);
                continue;
            case '*':
                if(*pattern == '*') {
                    ++pattern;
                    push(cursor, '.');
                }
                else push_string(cursor, "[^\\/]", 5);
                push(cursor, '*');
                continue;
            case '(':
                push(cursor, '(');
                goto OPTIONAL;
            case '[':
                push(cursor, '[');
                goto CLASS;
            case '{':
                push(cursor, '(');
                goto ALTERNATIVE;
            case '\\':
                if(!*pattern) return NULL;
                push_escape(cursor, *pattern++);
                continue;
            default:
                push_encoded(cursor, cc);
                continue;
        }
    }

    OPTIONAL: for(;;) {
        cc = *pattern++;
        switch(cc) {
            case 0:
                return NULL;
            case ')':
                push(cursor, ')');
                push(cursor, '?');
                goto NEXT;
            case '\\': {
                if(!*pattern) return NULL;
                push_escape(cursor, *pattern++);
                continue;
            }
            default:
                push_encoded(cursor, cc);
                continue;
        }
    }

    CLASS: for(;;) {
        cc = *pattern++;
        switch(cc) {
            case 0:
                return NULL;
            case ']':
                push(cursor, ']');
                goto NEXT;
            case '^':
            case '-':
                push(cursor, cc);
                continue;
            case '\\':
                if(!*pattern) return NULL;
                push_escape(cursor, *pattern++);
                continue;
            default:
                push_encoded(cursor, cc);
                continue;
        }
    }

    ALTERNATIVE: for(;;) {
        cc = *pattern++;
        switch(cc) {
            case 0:
                return NULL;
            case '}':
                if(*pattern == '{') {
                    ++pattern;
                    push(cursor, '|');
                    continue;
                }
                push(cursor, ')');
                goto NEXT;
            case '\\': {
                if(!*pattern) return NULL;
                cc = *pattern++;
                push_escape(cursor, *pattern++);
                continue;
            }
            default:
                push_encoded(cursor, cc);
                continue;
        }
    }
}
