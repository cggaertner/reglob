#ifndef REGLOB_H_
#define REGLOB_H_

#include <stddef.h>

#define REGLOB_MAX_SIZE(len) (5 * (len) + 3)

char *reglob(char *buffer, const char *pattern, _Bool root);

#endif
