#include "reglob.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
    static char re[1024];

    for(int i = 1; i < argc; ++i)
        printf("%s\n", reglob(re, argv[i], 0));

    return 0;
}
